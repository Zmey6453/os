﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using img_processing_shared_dll;
using img_processing_shared_dll.Filter;

namespace img_processing_client
{
    class ImageProcessingClient : BaseClient
    {


        public ImageProcessingClient() : base()
        {

        }

        public bool IsConnected
        {
            get
            {
                return Client.Connected;
            }
        }

        public bool TryToConnect(IPEndPoint endPoint)
        {
            try
            {
                Client.Connect(endPoint); 
                Console.WriteLine("Connection was successful");
            }
            catch
            {
                Console.WriteLine("Connection was rejected");
                return false;
            }
            byte[] temp = new byte[16];
            Client.GetStream().Read(temp, 0, 16);
            Guid = new Guid(temp);
            return true;
        }

        public ImageProcessingMessage WaitForMessage()
        {
            Client.GetStream().Read(Buffer, 0, 4);
            
            Console.WriteLine("Reading");
            int length = BitConverter.ToInt32(Buffer, 0);
            if (length == -1) return null; //пришло сообщение, что сервер мертв

            int currentLength = 0;

            do
            {

                currentLength += Client.GetStream().Read(Buffer, currentLength, length - currentLength);

            }
            while (currentLength < length);

            //Console.WriteLine("currentLength {0} length {1} ", currentLength, length);
            Console.WriteLine("Reading done");

            return new ImageProcessingMessage(Buffer, length);
        }
    }
}


