﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using img_processing_shared_dll;
using img_processing_shared_dll.Filter;

namespace img_processing_client
{
    class Program
    {
        static readonly IPEndPoint EndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 30687); //127.0.0.1

        static void Main(string[] args)
        {
            ImageProcessingClient client = new ImageProcessingClient();
            while (!client.IsConnected)
            {
                client.TryToConnect(EndPoint);
                Console.WriteLine($"Appointed GUID: {client.Guid}");
            }

            while (true)
            {
                ImageProcessingMessage message = client.WaitForMessage();
                if (message == null) return; //пришло сообщение, что сервер мертв

                Console.WriteLine("Processing");
                OnePixelFilter filter = OnePixelFilter.GetFilterByID(message.FilterID);
                for (int i = 0; i < message.ImageArray.Length; i++)
                {
                    message.ImageArray[i] = filter.Filter(message.ImageArray[i]);
                }
                Console.WriteLine("Processing done");
                client.SendMessage(message);
            }
        }
    }
}
