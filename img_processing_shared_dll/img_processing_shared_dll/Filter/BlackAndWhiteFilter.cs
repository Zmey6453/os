﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    class BlackAndWhiteFilter : OnePixelFilter
    {
        public BlackAndWhiteFilter()
        {
            FilterID = 2;
            FilterName = "Black and white";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);
            byte val = (byte)((c.R + c.G + c.B) / 3);
            if (val < 128) return 0 + (c.A << 24);
            else
                return 0xFFFFFF + (c.A << 24);
        }
    }
}
