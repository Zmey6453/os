﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    class ContrastFilter : OnePixelFilter
    {
        public ContrastFilter()
        {
            FilterID = 4;
            FilterName = "Increase contrast";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);
            byte bottom = 80, top = 175;
            byte val = (byte)((c.R + c.G + c.B) / 3);
            if (val < bottom) return 0 + (c.A << 24);
            if (val > top) return 0xFFFFFF + (c.A << 24);

            return incomingPixel;
        }
    }
}
