﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    //интерфейс для фильтра с обработкой одного пиксела (0 соседей)
    public interface IOnePixelFilter : IFilter
    {
        int Filter(int incomingPixel);
    }
}
