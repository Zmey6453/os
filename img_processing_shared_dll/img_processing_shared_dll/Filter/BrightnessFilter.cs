﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    class BrightnessFilter : OnePixelFilter
    {
        public byte K { get; private set; } = 20;
        public BrightnessFilter()
        {
            FilterID = 5;
            FilterName = "Increase brightness";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);
           
            return Border(c.B) + (Border(c.G) << 8) + (Border(c.R) << 16) + (c.A << 24);

        }

        private byte Border(int v) {

            v += K;
            if (v > 255) return 0xFF;
            else return (byte)v;
        }
    }
}
