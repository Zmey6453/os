﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{

    //Класс-обёртка для фильтров
    public abstract class OnePixelFilter : IOnePixelFilter
    {
        //номер фильтра
        public int FilterID { get; set; }
        //имя фильтра
        public string FilterName { get; set; }

        //абстрактный метод перобразования 
        public abstract int Filter(int incomingPixel);

        public override string ToString()
        {
            return FilterName;
        }

        //статический метод для получения фильтра по ID
        public static OnePixelFilter GetFilterByID(int id)
        {
            return AllFilters.Where((OnePixelFilter f) => { return f.FilterID == id; }).First();
        }

        //статический метод для получения всех фильтров 
        public static IReadOnlyList<OnePixelFilter> GetAllFilters()
        {
            return AllFilters.AsReadOnly();
        }

        //все возможные фильтры
        private static readonly List<OnePixelFilter> AllFilters = new List<OnePixelFilter>() { new EqualFilter(), new ColoredToGreyFilter(), new BlackAndWhiteFilter(), new GreenFilter(), new ContrastFilter(), new BrightnessFilter(), new ShiftFilter()};
    }
}
