﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    class GreenFilter : OnePixelFilter
    {
        public GreenFilter()
        {
            FilterID = 3;
            FilterName = "Green";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);


            byte val = (byte)((c.R + c.G + c.B) / 3);
            return val + (Border(val * 2) << 8) + ((Border((int)(val * 1.2))) << 16) + (c.A << 24);

        }

        private byte Border(int v)
        {
            if (v > 255) return 0xFF;
            else return (byte)v;
        }
    }
}
