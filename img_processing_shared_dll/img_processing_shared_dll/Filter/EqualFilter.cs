﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{

    //Эквивалентный фильтр (служит для теста)
    class EqualFilter : OnePixelFilter
    {
        public EqualFilter()
        {
            FilterID = 0;
            FilterName = "Equal";
        }
        public override int Filter(int incomingPixel)
        {
            return incomingPixel;
        }

    }
}
