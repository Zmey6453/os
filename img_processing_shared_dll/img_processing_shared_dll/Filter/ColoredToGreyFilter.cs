﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{

    public class ColoredToGreyFilter : OnePixelFilter
    {
        public ColoredToGreyFilter()
        {
            FilterID = 1;
            FilterName = "Colored to grey";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);
            byte val = (byte)((c.R + c.G + c.B) / 3);
            return val + (val << 8) + (val << 16) + (c.A << 24);
        }
    }
}
