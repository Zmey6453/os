﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace img_processing_shared_dll.Filter
{
    class ShiftFilter : OnePixelFilter
    {
        public ShiftFilter()
        {
            FilterID = 6;
            FilterName = "Shift colors";
        }

        public override int Filter(int incomingPixel)
        {
            Color c = new Color(incomingPixel);
            if (c.R == c.G && c.G == c.B) return incomingPixel;

            return c.G + (c.R << 8) + (c.B << 16) + (c.A << 24);
           
        }
    }
}
