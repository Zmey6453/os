﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Net.Sockets;
using System.Net;
using img_processing_shared_dll;
using System.Windows.Forms;
using System.Threading;

namespace img_processing_server
{
    class ImageProcessingServer : IProcessing
    {
        
        private static class Config
        {
            public static readonly int Port = 30687;
            public static readonly IPAddress Ip = IPAddress.Parse("127.0.0.1"); //127.0.0.1
        }
        //мьютекс на вставку в список
        private readonly Mutex mutex;
        //мьютекс на декремент поля
        private readonly Mutex onDecrement;

        //временное поле, хранит счётчик оставшихся кусков
        public int PartsRemained { get; private set; }

        //слушатель входящих соединений
        protected TcpListener Listener { get; private set; }
        //список всех peer'ов
        private List<BaseClient> Workers { get; set; }

        //временный буфер для картинки
        private int[] ArrayTempBuffer { get; set; }
        //смещение (оно же кусок работы для всех)
        private int Offset { get; set; }
        //временная переменная, хранящая делегат для вызова
        OnImageProcessed onImageProcessed;

        public ImageProcessingServer()
        {
            Listener = new TcpListener(Config.Ip, Config.Port);
            Workers = new List<BaseClient>();
            mutex = new Mutex();
            onDecrement = new Mutex();
            Startup();
        }

        //начинаем слушать в отдельной нити
        private async void Startup()
        {
            Listener.Start();
            while (true)
            {
                TcpClient client = await Listener.AcceptTcpClientAsync();
                if (mutex.WaitOne())
                {
                    BaseClient newClient = new BaseClient(Guid.NewGuid(), client);

                    byte[] bytes = newClient.Guid.ToByteArray();
                    newClient.Client.GetStream().Write(bytes, 0, bytes.Length);
                    Workers.Add(newClient);
                    mutex.ReleaseMutex();
                }
            }
        }

        public void Stop()
        {
            foreach (BaseClient client in Workers)
            {
                client.Client.GetStream().Write(BitConverter.GetBytes(-1), 0, 4);
            }
            Workers.Clear();
        }

        //получить массив интов по картинке
        private int[] GetImageArray(Bitmap image)
        {
            int[] array = new int[image.Width * image.Height];
            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    array[i * image.Width + j] = image.GetPixel(j, i).ToArgb();
                }
            }
            return array;
        }
        //обработать картинку
        public void ProcessImage(Bitmap image, int filterID, OnImageProcessed onImageProcessed)
        {
            this.onImageProcessed = onImageProcessed;
            if (Workers.Count == 0)
            {
                MessageBox.Show("At least 1 client must be connected", "Error");
            }
            else
            {
                //высчитываем смещение, сколько частей будем ждать и тд
                int[] imageArray = GetImageArray(image);
                ArrayTempBuffer = imageArray;
                int workerOffset = imageArray.Length / Workers.Count;
                PartsRemained = Workers.Count;
                Offset = workerOffset;
                int counter = 0;
                
                foreach (BaseClient client in Workers)
                {
                    
                    ImageProcessingMessage message = new ImageProcessingMessage(counter++, filterID, image.Height, image.Width, workerOffset, imageArray);                    
                    Task.Factory.StartNew(() => client.SendMessage(message));                    
                    client.Client.GetStream().BeginRead(client.Buffer, 0, 4, OnReadFromNetworkStream, client);
                }
            }

        }

        // по получении ответа 
        private void OnReadFromNetworkStream(IAsyncResult result)
        {
            BaseClient client = result.AsyncState as BaseClient;
            int msgLength = BitConverter.ToInt32(client.Buffer, 0);
            int currentLength = 0;
            
            do
            {
                currentLength += client.Client.GetStream().Read(client.Buffer, currentLength, msgLength - currentLength);
            }
            while (currentLength < msgLength);
           
            ImageProcessingMessage msg = new ImageProcessingMessage(client.Buffer, msgLength);

            //присваиваем значения в соответствии с workID и смещением
            for (int i = 0; i < msg.ImageArray.Length; i++)
            {
                ArrayTempBuffer[i + (msg.WorkID * Offset)] = msg.ImageArray[i];
            }
           
            if (onDecrement.WaitOne())
            {
                PartsRemained--;
               
                if (PartsRemained == 0)
                {
                    //вызов делегата
                    onImageProcessed(ArrayTempBuffer);
                }
                onDecrement.ReleaseMutex();
            }
           
            client.Client.GetStream().EndRead(result);
        }
    }
}

