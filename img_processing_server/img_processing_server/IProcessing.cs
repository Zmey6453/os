﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace img_processing_server
{
    //делегат, вызываемый после обработки изображения 
    delegate void OnImageProcessed(int[] image);
   
    interface IProcessing
    {
        void ProcessImage(Bitmap image, int filterID, OnImageProcessed onImageProcessed);
    }
}
