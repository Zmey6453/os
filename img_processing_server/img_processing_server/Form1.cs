﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using img_processing_shared_dll.Filter;

namespace img_processing_server
{
    public partial class Form1 : Form
    {
        private string CurrentFileName { get; set; }
        private int CurrentFilterID { get; set; }

        private readonly ImageProcessingServer processingServer;
        public Form1()
        {
            InitializeComponent();
            processingServer = new ImageProcessingServer();
            processButton.Enabled = false;
            //заполняем выпадающий список
            filterList.Items.AddRange(OnePixelFilter.GetAllFilters().ToArray());
            filterList.SelectedIndex = 0;
        }

        private void OnFilterSelect(object sender, EventArgs e)
        {
            CurrentFilterID = ((OnePixelFilter)filterList.SelectedItem).FilterID;
        }

        private void OnImageSelect(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                
                dialog.Filter = ".jpeg(*.jpeg)|*.jpeg|.jpg(*.jpg)|*.jpg|.bmp(*.bmp)|*.bmp|.png(*.png)|*.png|All files (*.*)|*.*";
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    CurrentFileName = dialog.FileName;
                    try
                    {
                        pictureBox.Image = new Bitmap(CurrentFileName);
                        processButton.Enabled = true;
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show("Unavailable format", "Error");
                        processButton.Enabled = false;
                    }
                }
            }
        }
        //метод завершения обработки картинки
        private void OnImageHandled(int[] img)
        {
            BeginInvoke((MethodInvoker)(() => {
                Bitmap b = new Bitmap(pictureBox.Image);
                int H = pictureBox.Image.Size.Height;
                int W = pictureBox.Image.Size.Width;
                for (int i = 0; i < H; i++)
                {
                    for (int j = 0; j < W; j++)
                    {
                        b.SetPixel(j, i, Color.FromArgb(img[i * pictureBox.Image.Width + j]));
                    }
                }
                pictureBox.Image = b;
                processButton.Enabled = true;
            }));
        }
        //по нажатии на кнопку блокируем её, вызываем обработчик картинки
        private void OnProcessButtonClick(object sender, EventArgs e)
        {
            processButton.Enabled = false;
            Task.Factory.StartNew(
                () => processingServer.ProcessImage(pictureBox.Image as Bitmap, CurrentFilterID, OnImageHandled));
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            //сообщение клиентам
            processingServer.Stop();
        }
    }
}
