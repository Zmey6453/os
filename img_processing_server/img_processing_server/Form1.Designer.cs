﻿namespace img_processing_server
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.selectImage = new System.Windows.Forms.Button();
            this.processButton = new System.Windows.Forms.Button();
            this.filterList = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(12, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1800, 860);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // selectImage
            // 
            this.selectImage.Location = new System.Drawing.Point(12, 909);
            this.selectImage.Name = "selectImage";
            this.selectImage.Size = new System.Drawing.Size(114, 32);
            this.selectImage.TabIndex = 1;
            this.selectImage.Text = "Select image";
            this.selectImage.UseVisualStyleBackColor = true;
            this.selectImage.Click += new System.EventHandler(this.OnImageSelect);
            // 
            // processButton
            // 
            this.processButton.Location = new System.Drawing.Point(12, 947);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(114, 29);
            this.processButton.TabIndex = 2;
            this.processButton.Text = "Start process";
            this.processButton.UseVisualStyleBackColor = true;
            this.processButton.Click += new System.EventHandler(this.OnProcessButtonClick);
            // 
            // filterList
            // 
            this.filterList.BackColor = System.Drawing.SystemColors.MenuBar;
            this.filterList.FormattingEnabled = true;
            this.filterList.Location = new System.Drawing.Point(153, 912);
            this.filterList.Name = "filterList";
            this.filterList.Size = new System.Drawing.Size(174, 24);
            this.filterList.TabIndex = 3;
            this.filterList.SelectedIndexChanged += new System.EventHandler(this.OnFilterSelect);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.filterList);
            this.Controls.Add(this.processButton);
            this.Controls.Add(this.selectImage);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button selectImage;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.ComboBox filterList;
    }
}

